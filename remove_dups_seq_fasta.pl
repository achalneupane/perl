use strict;
use Bio::SeqIO;
my %unique;

##Usage: perl remove_dups_seq_fasta.pl input_fasta_with_duplicate_sequences.fasta

#my $file   = "/media/owner/b45f8e7a-003c-4573-8841-bcb5f76f281f/analysis/ALL_endorna_virus_with_ncbi_endornavirus_all_longer_than_4000aa_full_length_non_hypothetical.fasta";
my $file = $ARGV[0];
my $seqio  = Bio::SeqIO->new(-file => $file, -format => "fasta");
my $outseq = Bio::SeqIO->new(-file => ">$file.uniq", -format => "fasta");

while(my $seqs = $seqio->next_seq) {
  my $id  = $seqs->display_id;
  my $seq = $seqs->seq;
  unless(exists($unique{$seq})) {
    $outseq->write_seq($seqs);
    $unique{$seq} +=1;
  }
}
